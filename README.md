# Slim Python

Default `python:3.8-slim` image with added:
 - the latest `pip`
 - `gcc` package (for [ciso8601](https://pypi.org/project/ciso8601/) support)
 - Moscow timezone (UTC+3)
